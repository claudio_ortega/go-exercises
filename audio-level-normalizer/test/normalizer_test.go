package test

import (
	"audionormalizer"
	"fmt"
	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"
	"os"
	"path/filepath"
	"testing"
)

func TestNormalizer(t *testing.T) {
	t.Run("help", func(t *testing.T) {
		args := []string{"app", "-h"}
		err := audionormalizer.MainAlt(args)
		assert.NoError(t, err)
	})

	{
		dirOut := lo.Must(filepath.Abs("audio-files/output"))
		err := os.RemoveAll(dirOut)
		assert.NoError(t, err)
	}

	dirInput := "audio-files/input/1"
	dirOutputWav := "audio-files/output/wav"
	dirOutputWavNorm := "audio-files/output/wav-norm"

	//dirInput := "/Users/cortega/Desktop/tmp/input"
	//dirOutputWav := "/Users/cortega/Desktop/tmp/output/wav"
	//dirOutputWavNorm := "/Users/cortega/Desktop/tmp/output/wav-norm"

	//dirInput := "/Volumes/Samsung_T5/tandas/nueva"
	//dirOutputWav := "/Volumes/Samsung_T5/tandas/output/nueva-wav"
	//dirOutputWavNorm := "/Volumes/Samsung_T5/tandas/output/nueva-wav-norm"

	//dirInput := "/Users/cortega/Desktop/nueva-mirrored-from-google"
	//dirOutputWav := "/Users/cortega/Desktop/tmp-remove-me-nueva-wav"
	//dirOutputWavNorm := "/Users/cortega/Desktop/nueva-wav-norm"

	t.Run("convert to .wav", func(t *testing.T) {
		args := []string{
			"app",
			"--dry-run=false",
			"--operation=convert_to_wav",
			fmt.Sprintf("--source-dir=%s", dirInput),
			fmt.Sprintf("--target-dir=%s", dirOutputWav),
		}
		err := audionormalizer.MainAlt(args)
		assert.NoError(t, err)
	})

	t.Run("measure before normalization", func(t *testing.T) {
		args := []string{
			"app",
			"--dry-run=false",
			"--operation=measure_levels",
			fmt.Sprintf("--source-dir=%s", dirOutputWav),
		}
		err := audionormalizer.MainAlt(args)
		assert.NoError(t, err)
	})

	t.Run("normalize", func(t *testing.T) {
		args := []string{
			"app",
			"--dry-run=false",
			"--operation=normalize",
			"--normalization=normalize_with_mean",
			"--reference-level=-20",
			fmt.Sprintf("--source-dir=%s", dirOutputWav),
			fmt.Sprintf("--target-dir=%s", dirOutputWavNorm),
		}
		err := audionormalizer.MainAlt(args)
		assert.NoError(t, err)
	})

	t.Run("measure after normalization", func(t *testing.T) {
		args := []string{
			"app",
			"--dry-run=false",
			"--operation=measure_levels",
			fmt.Sprintf("--source-dir=%s", dirOutputWavNorm),
		}
		err := audionormalizer.MainAlt(args)
		assert.NoError(t, err)
	})
}
