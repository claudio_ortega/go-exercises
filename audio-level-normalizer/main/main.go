package main

import (
	"audionormalizer"
	"log"
	"os"
)

func main() {
	exitCode := 0
	if err := audionormalizer.MainAlt(os.Args); err != nil {
		log.Println(err.Error())
		exitCode = 1
	}
	os.Exit(exitCode)
}
