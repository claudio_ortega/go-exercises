package audionormalizer

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/samber/lo"
	"github.com/spf13/pflag"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
)

type (
	filePathT               string
	fileNameFilterFunctionT func(path filePathT) bool
	visitorFunctionT        func(
		filePath filePathT,
		info os.FileInfo,
		stats *statsT,
	) error
	statsT struct {
		Files            int
		MatchedFiles     int
		UnprocessedFiles int
		ErrorCount       int
	}
	operationT string
	levelsT    struct {
		max          float64
		mean         float64
		clippingRate float64
		clipping     bool
	}
	normalizationT string
)

const (
	operationConvertToWav  = operationT("convert_to_wav")
	operationMeasureLevels = operationT("measure_levels")
	operationNormalize     = operationT("normalize")
)

var operations = []operationT{
	operationConvertToWav,
	operationMeasureLevels,
	operationNormalize,
}

const (
	normalizeWithMean = normalizationT("normalize_with_mean")
	normalizeWithMax  = normalizationT("normalize_with_max")
)

var normalizations = []normalizationT{
	normalizeWithMean,
	normalizeWithMax,
}

func MainAlt(args []string) error {
	var (
		help               bool
		dryRun             bool
		sourceDir          string
		targetDir          string
		operation          string
		referenceLevelInDB float64
		normalization      string
	)

	flags := pflag.NewFlagSet(args[0], pflag.ExitOnError)
	flags.BoolVarP(&help, "help", "h", false, "give help and quit")
	flags.BoolVarP(&dryRun, "dry-run", "d", false, "show what could be done, without doing it")
	flags.StringVarP(&sourceDir, "source-dir", "s", "", "source directory")
	flags.StringVarP(&targetDir, "target-dir", "t", "", "target directory")
	flags.StringVarP(&operation, "operation", "o", string(operationMeasureLevels), fmt.Sprintf("one of: %v", operations))
	flags.Float64VarP(&referenceLevelInDB, "reference-level", "r", 0, "reference level in dB")
	flags.StringVarP(&normalization, "normalization", "n", string(normalizeWithMax), fmt.Sprintf("one of: %v", normalizations))

	if err := flags.Parse(args[1:]); err != nil {
		flags.Usage()
		return err
	}

	if help {
		flags.PrintDefaults()
		return nil
	}

	_, _, err := executeUsingBash("ffmpeg -version")
	if err != nil {
		return errors.New("ffmpeg is not installed, use 'brew install ffmpeg'")
	}

	sourceDir = lo.Must(filepath.Abs(sourceDir))
	targetDir = lo.Must(filepath.Abs(targetDir))

	var (
		stats   statsT
		walkErr error
	)

	switch operationT(operation) {
	case operationConvertToWav:
		walkErr = walk(
			filePathT(sourceDir),
			func(sourceFilePath filePathT, info os.FileInfo, stats *statsT) error {
				targetFilePath := lo.Must(referFilePath(filePathT(sourceDir), filePathT(targetDir), sourceFilePath))
				targetFilePath = replaceAnyTermination(targetFilePath, []string{".mp3", ".m4a"}, ".wav")
				return convertFormats(dryRun, sourceFilePath, targetFilePath)
			},
			func(filePath filePathT) bool {
				ext := filepath.Ext(string(filePath))
				return lo.Contains([]string{".mp3", ".m4a", ".wav"}, ext)
			},
			&stats,
		)
	case operationMeasureLevels:
		walkErr = walk(
			filePathT(sourceDir),
			func(filePath filePathT, info os.FileInfo, stats *statsT) error {
				levels, err := measureLevels(dryRun, filePath)
				if err == nil {
					fmt.Printf("file:[%s], levels:%v\n", filePath, levels)
				}
				return err
			},
			func(filePath filePathT) bool {
				ext := filepath.Ext(string(filePath))
				return ext == ".wav"
			},
			&stats,
		)
	case operationNormalize:
		walkErr = walk(
			filePathT(sourceDir),
			func(sourceFilePath filePathT, info os.FileInfo, stats *statsT) error {
				targetFilePath := lo.Must(referFilePath(filePathT(sourceDir), filePathT(targetDir), sourceFilePath))
				return normalizeLevel(
					dryRun,
					sourceFilePath,
					targetFilePath,
					normalizationT(normalization),
					referenceLevelInDB,
					stats)
			},
			func(filePath filePathT) bool {
				return true
			},
			&stats,
		)
	default:
		return fmt.Errorf("unknown operation: [%s]", operation)
	}

	fmt.Printf("stats:%s\n", string(lo.Must(json.Marshal(stats))))

	return walkErr
}

func walk(
	filePath filePathT,
	visitFn visitorFunctionT,
	filterFn fileNameFilterFunctionT,
	stats *statsT,
) error {
	info, err := os.Lstat(string(filePath))
	if err != nil {
		return err
	}
	if info.IsDir() {
		// is a directory, just walk into each child
		for _, childName := range lo.Must(listFilesUnderDir(filePath)) {
			child := filepath.Join(string(filePath), string(childName))
			if err = walk(filePathT(child), visitFn, filterFn, stats); err != nil {
				return err
			}
		}
	} else {
		// is a plain file
		stats.Files++
		if !filterFn(filePath) {
			return nil
		}
		stats.MatchedFiles++
		err1 := visitFn(filePath, info, stats)
		if err1 != nil {
			stats.ErrorCount = stats.ErrorCount + 1
		}
		return err1
	}
	return nil
}

func listFilesUnderDir(dirname filePathT) ([]filePathT, error) {
	f := lo.Must(os.Open(string(dirname)))
	defer func() {
		_ = f.Close()
	}()
	names := lo.Must(f.Readdirnames(-1))
	return lo.Map(names, func(item string, _ int) filePathT {
		return filePathT(item)
	}), nil
}

func replaceAnyTermination(
	filePath filePathT,
	replaceFrom []string,
	replaceTo string,
) filePathT {
	tmp := string(filePath)
	for _, termination := range replaceFrom {
		tmp = strings.Replace(tmp, termination, replaceTo, 1)
	}
	return filePathT(tmp)
}

func convertFormats(
	dryRun bool,
	sourceFilePath filePathT,
	targetFilePath filePathT,
) error {
	cmd := fmt.Sprintf("ffmpeg -y -i \"%s\" \"%s\"", sourceFilePath, targetFilePath)
	return createTargetDirAndExecute(dryRun, targetFilePath, cmd)
}

func normalizeLevel(
	dryRun bool,
	sourceFilePath filePathT,
	targetFilePath filePathT,
	normalization normalizationT,
	refLevel float64,
	stats *statsT,
) error {
	_, err := os.Lstat(string(targetFilePath))
	if err == nil {
		fmt.Printf("skipping existing output file: [%s]\n", targetFilePath)
		stats.UnprocessedFiles = stats.UnprocessedFiles + 1
		return nil
	}

	if strings.Contains(string(sourceFilePath), "silence-") {
		if err := os.MkdirAll(filepath.Dir(string(targetFilePath)), 0755); err != nil {
			return err
		}
		_, _, err := executeUsingBash(fmt.Sprintf("cp \"%s\" \"%s\"", sourceFilePath, targetFilePath))
		return err
	}

	levels := lo.Must(measureLevels(dryRun, sourceFilePath))
	var adjustment float64
	if normalization == normalizeWithMax {
		adjustment = refLevel - levels.max
	} else if normalization == normalizeWithMean {
		adjustment = refLevel - levels.mean
	} else {
		return fmt.Errorf("wrong normalization: %s", normalization)
	}
	if adjustment > 0 {
		fmt.Printf("normalized to higher volume, adjustment:%.2fdB, file:%s\n", adjustment, targetFilePath)
	}
	cmd := fmt.Sprintf("ffmpeg -y -i \"%s\" -af \"volume=%.4fdB\" \"%s\"",
		sourceFilePath,
		adjustment,
		targetFilePath,
	)
	return createTargetDirAndExecute(dryRun, targetFilePath, cmd)
}

func createTargetDirAndExecute(
	dryRun bool,
	targetFilePath filePathT,
	cmd string,
) error {
	if dryRun {
		doDryRun(cmd)
		return nil
	}
	if err := os.MkdirAll(filepath.Dir(string(targetFilePath)), 0755); err != nil {
		return err
	}
	_, _, err := executeUsingBash(cmd)
	if err != nil {
		fmt.Printf("error processing on command:[%s], err:[%s]\n", cmd, err.Error())
	}
	return err
}

func measureLevels(
	dryRun bool,
	filePath filePathT,
) (levelsT, error) {
	cmd := fmt.Sprintf("ffmpeg -i \"%s\" -af volumedetect -f null /dev/null", filePath)
	if dryRun {
		doDryRun(cmd)
		return levelsT{}, nil
	}
	_, stderr, err := executeUsingBash(cmd)
	if err != nil {
		return levelsT{}, err
	}
	clippingRate := extractClippingAt0db(stderr)
	levels := levelsT{
		max:          extractMax(stderr),
		mean:         extractMean(stderr),
		clippingRate: clippingRate,
		clipping:     clippingRate > 0.001,
	}
	return levels, nil
}

func doDryRun(cmd string) {
	fmt.Printf("dry_run [%s]\n", cmd)
}

func executeUsingBash(command string) (string, string, error) {
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd := exec.Command("bash", "-c", command)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	return stdout.String(), stderr.String(), err
}

func referFilePath(
	sourceDir filePathT,
	targetDir filePathT,
	sourceFilePath filePathT,
) (filePathT, error) {
	relPath, err := filepath.Rel(string(sourceDir), string(sourceFilePath))
	if err != nil {
		return "", err
	}
	targetFilePath := filepath.Join(string(targetDir), relPath)
	return filePathT(targetFilePath), nil
}

func extractMax(in string) float64 {
	return lo.Must(strconv.ParseFloat(lo.Must(extractInBetween(in, "max_volume:", "dB")), 64))
}

func extractMean(in string) float64 {
	return lo.Must(strconv.ParseFloat(lo.Must(extractInBetween(in, "mean_volume:", "dB")), 64))
}

func extractClippingAt0db(in string) float64 {
	histogram0dbSt, err := extractInBetween(in, "histogram_0db:", "\n")
	if err != nil {
		return 0
	}
	histogram0db := lo.Must(strconv.ParseFloat(histogram0dbSt, 64))

	_, afterFirstNSamples, found := strings.Cut(in, "n_samples:")
	if !found {
		panic("n_samples word was not found (1)")
	}

	nSamplesSt, err := extractInBetween(afterFirstNSamples, "n_samples:", "\n")
	if err != nil {
		panic("n_samples word was not found (2)")
	}
	nSamples := lo.Must(strconv.ParseFloat(nSamplesSt, 64))
	return histogram0db / nSamples
}

func extractInBetween(in string, left string, right string) (string, error) {
	_, after, found := strings.Cut(in, left)
	if !found {
		return "", fmt.Errorf("%s not found", left)
	}
	before, _, found := strings.Cut(after, right)
	if !found {
		return "", fmt.Errorf("%s not found", right)
	}
	return strings.Replace(before, " ", "", -1), nil
}
