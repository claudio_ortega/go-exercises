package stub_test

import (
	"errors"
	"fmt"
	"test_examples/stub"
	"testing"

	"github.com/google/go-cmp/cmp"
)

type GetPetNamesStub struct {
	stub.Entities
}

func (ps GetPetNamesStub) GetPets(userID string) ([]stub.Pet, error) {
	switch userID {
	case "1":
		return []stub.Pet{{Name: "Bubbles"}}, nil
	case "2":
		return []stub.Pet{{Name: "Stampy"}, {Name: "Snowball II"}}, nil
	default:
		return nil, fmt.Errorf("invalid id: %s", userID)
	}
}

func TestLogicGetPetNames(t *testing.T) {

	data := []struct {
		name     string
		userID   string
		petNames []string
	}{
		{"case1", "1", []string{"Bubbles"}},
		{"case2", "2", []string{"Stampy", "Snowball II"}},
		{"case3", "3", nil},
	}

	l := stub.Logic{
		GetPetNamesStub{},
	}

	for _, d := range data {
		t.Run(d.name, func(t *testing.T) {
			petNames, err := l.GetPetNames(d.userID)
			if err != nil {
				t.Error(err)
			}
			if diff := cmp.Diff(d.petNames, petNames); diff != "" {
				t.Error(diff)
			}
		})
	}
}

type EntitiesStub struct {
	getUser     func(id string) (stub.User, error)
	getPets     func(userID string) ([]stub.Pet, error)
	getChildren func(userID string) ([]stub.Person, error)
	getFriends  func(userID string) ([]stub.Person, error)
	saveUser    func(user stub.User) error
}

func (es EntitiesStub) GetUser(id string) (stub.User, error) {
	return es.getUser(id)
}

func (es EntitiesStub) GetPets(userID string) ([]stub.Pet, error) {
	return es.getPets(userID)
}

func (es EntitiesStub) GetChildren(userID string) ([]stub.Person, error) {
	return es.getChildren(userID)
}

func (es EntitiesStub) GetFriends(userID string) ([]stub.Person, error) {
	return es.getFriends(userID)
}

func (es EntitiesStub) SaveUser(user stub.User) error {
	return es.saveUser(user)
}

func TestLogicGetPetNames2(t *testing.T) {

	data := []struct {
		name     string
		getPets  func(userID string) ([]stub.Pet, error)
		userID   string
		petNames []string
		errMsg   string
	}{
		{
			name: "case1",
			getPets: func(userID string) ([]stub.Pet, error) {
				return []stub.Pet{
					{Name: "Bubbles"},
				}, nil
			},
			userID:   "1",
			petNames: []string{"Bubbles"},
			errMsg:   "",
		},

		{
			name: "case2",
			getPets: func(userID string) ([]stub.Pet, error) {
				return []stub.Pet{
					{Name: "Stampy"},
					{Name: "Snowball II"},
				}, nil
			},
			userID: "2",
			petNames: []string{
				"Stampy",
				"Snowball II",
			},
			errMsg: "",
		},

		{
			name: "case3",
			getPets: func(userID string) ([]stub.Pet, error) {
				return nil, errors.New("invalid id: 3")
			},
			userID:   "3",
			petNames: nil,
			errMsg:   "invalid id: 3",
		},
	}

	l := stub.Logic{}

	for _, d := range data {

		t.Run(
			d.name,
			func(t *testing.T) {
				l.Entities = EntitiesStub{getPets: d.getPets}
				petNames, err := l.GetPetNames(d.userID)
				if diff := cmp.Diff(petNames, d.petNames); diff != "" {
					t.Error(diff)
				}
				var errMsg string
				if err != nil {
					errMsg = err.Error()
				}
				if errMsg != d.errMsg {
					t.Errorf("Expected error [%s], got [%s]", d.errMsg, errMsg)
				}
			},
		)

	}
}
