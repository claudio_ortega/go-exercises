package main

import (
	"encoding/json"
	"fmt"
	xj "github.com/basgys/goxml2json"
	"github.com/samber/lo"
	"log"
	"math"
	"os"
	"path"
	"path/filepath"
	"slices"
	"strconv"
	"strings"
	"time"
)

func main() {
	exitCode := 0
	if err := mainAlt(os.Args); err != nil {
		log.Printf("error:%s\n", err.Error())
		exitCode = 1
	}
	os.Exit(exitCode)
}

func mainAlt(_ []string) error {
	const basedirPath = "/Users/cortega/Desktop"
	files, err := os.ReadDir(basedirPath)
	if err != nil {
		return err
	}
	files = lo.FilterMap(files, func(file os.DirEntry, _ int) (os.DirEntry, bool) {
		return file, path.Ext(file.Name()) == ".xspf"
	})
	if len(files) == 0 {
		fmt.Println("no files to report on")
		return nil
	}
	for _, file := range files {
		absPath := filepath.Join(basedirPath, file.Name())
		fmt.Printf("reporting on file: %s\n\n", absPath)
		err = reportDurationsInFile(absPath)
		if err != nil {
			return err
		}
	}
	return nil
}

type songInfo struct {
	name     string
	duration time.Duration
}

type accumulator struct {
	songInfos []songInfo
}

func (a *accumulator) addSongInfo(
	songInfo songInfo,
) {
	a.songInfos = append(a.songInfos, songInfo)
}

type stats struct {
	mean time.Duration
	std  time.Duration
}

func (a stats) String() string {
	return fmt.Sprintf("mean:%s, std:%s", formatDuration(a.mean), formatDuration(a.std))
}

func computeStats(songInfos []songInfo) stats {
	var acum = uint64(0)
	for _, song := range songInfos {
		acum = acum + uint64(song.duration)
	}
	mean := acum / uint64(len(songInfos))

	variance := float32(0)
	for _, song := range songInfos {
		dev := float32(song.duration) - float32(mean)
		variance = variance + dev*dev
	}
	stDev := math.Sqrt(float64(variance / float32(len(songInfos))))

	return stats{
		mean: time.Duration(mean),
		std:  time.Duration(stDev),
	}
}

func (a *accumulator) print(min, max time.Duration) {
	sortedAndFiltered := lo.FilterMap(a.songInfos,
		func(item songInfo, _ int) (songInfo, bool) {
			return item, item.duration > min && item.duration < max
		})

	slices.SortStableFunc(
		sortedAndFiltered,
		func(a, b songInfo) int {
			return int(a.duration) - int(b.duration)
		})

	acumDuration := time.Duration(0)
	for i, song := range sortedAndFiltered {
		acumDuration = acumDuration + song.duration
		fmt.Printf("%4d %s %s %s\n",
			i+1,
			formatDuration(acumDuration),
			formatDuration(song.duration),
			song.name,
		)
	}

	fmt.Printf("\n%s\n\n", computeStats(sortedAndFiltered).String())
}

func reportDurationsInFile(filename string) error {
	content, err := os.ReadFile(filename)
	if err != nil {
		return err
	}

	xmlReader := strings.NewReader(string(content))
	jsonBuffer, err := xj.Convert(xmlReader)
	if err != nil {
		return err
	}

	var root map[string]interface{}
	err = json.Unmarshal(jsonBuffer.Bytes(), &root)
	if err != nil {
		return err
	}

	var acm accumulator
	err = visitMap(root, &acm)
	if err != nil {
		return err
	}

	acm.print(10*time.Second, 10*time.Minute)

	return nil
}

func visitMap(
	nodeMap map[string]interface{},
	accumulator *accumulator,
) error {
	for _, child := range lo.Values(nodeMap) {
		childNodeMap, ok := child.(map[string]interface{})
		if ok {
			err := visitMap(childNodeMap, accumulator)
			if err != nil {
				return err
			}
		}
		sliceOfInterfaces, ok := child.([]interface{})
		if ok {
			for _, oneInterface := range sliceOfInterfaces {
				childNode, ok1 := oneInterface.(map[string]interface{})
				if ok1 {
					name, ok2 := childNode["location"].(string)
					durationInMillisecond, ok3 := childNode["duration"].(string)
					if ok2 && ok3 {
						accumulator.addSongInfo(
							songInfo{
								name:     name,
								duration: time.Duration(lo.Must(strconv.Atoi(durationInMillisecond))) * time.Millisecond,
							})
					}
				}
			}
		}
	}
	return nil
}

func formatDuration(d time.Duration) string {
	secs := d.Nanoseconds() / 1_000_000_000
	minutes := secs / 60
	hours := minutes / 60
	return fmt.Sprintf("%02dh:%02dm:%02ds", hours, minutes%60, secs%60)
}
