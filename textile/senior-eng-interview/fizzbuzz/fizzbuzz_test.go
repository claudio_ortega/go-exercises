package fizzbuzz_test

import (
	"fmt"
	"testing"
)

func fizzbuzz(consumer func(int, string)) {
	for i := 0; i < 100; i++ {
		if i%3 == 0 && i%5 == 0 {
			consumer(i, "fizzbuzz")
		} else if i%3 == 0 {
			consumer(i, "fizz")
		} else if i%5 == 0 {
			consumer(i, "buzz")
		} else {
			// do nothing
		}
	}
}

func TestFizzBuzz(t *testing.T) {
	fizzbuzz(
		func(i int, s string) {
			fmt.Printf("%d - %s\n", i, s)
		},
	)
}
