module github.com/textileio/senior-eng-interview

go 1.15

require (
	github.com/ipfs/go-cid v0.0.7
	github.com/ipfs/go-ipfs-util v0.0.2
	github.com/minio/sha256-simd v0.1.1 // indirect
	github.com/mr-tron/base58 v1.2.0 // indirect
	github.com/multiformats/go-multihash v0.0.14 // indirect
	github.com/multiformats/go-varint v0.0.6 // indirect
	golang.org/x/crypto v0.0.0-20201203163018-be400aefbc4c // indirect
	golang.org/x/exp v0.0.0-20200331195152-e8c3332aa8e5
	golang.org/x/sys v0.0.0-20200420163511-1957bb5e6d1f // indirect
)
