# Senior Engineering Interview

## Setup

1. Share your screen.
2. Clone this repo.
3. Open this repo in your editor/IDE.

## FizzBuzz

For this exercise, navigate to the `fizzbuzz` folder and create a new code file in whatever language your prefer. 

Write a program that prints the numbers from 1 to 100. But for multiples of three print "Fizz" instead of the number and for the multiples of five print "Buzz". For numbers which are multiples of both three and five print "FizzBuzz".

## Record

Open up `record/record.go`.

1. Explain what you see in the file. What is this code doing? 
2. Run the tests in the `record` package.
3. `List` should fail on a disjoint sequence. Write a test for this.
4. Modify `Record` to allow for thread-safe calls to `Store`.
5. Write a test for concurrent calls to `Store`.
6. Write a new `Sequence` method that a caller can use to listen for new records as they arrive.
