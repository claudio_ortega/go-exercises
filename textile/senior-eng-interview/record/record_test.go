package record_test

import (
	"fmt"
	"github.com/ipfs/go-cid"
	util "github.com/ipfs/go-ipfs-util"
	. "github.com/textileio/senior-eng-interview/record"
	"golang.org/x/exp/rand"
	"strings"
	"sync"
	"testing"
)

type mockRecord struct {
	id, prevID cid.Cid
	data       []byte
}

func (r mockRecord) Cid() cid.Cid {
	return r.id
}

func (r mockRecord) PrevID() cid.Cid {
	return r.prevID
}

func TestRecord_SequenceConsistentSingleThread(t *testing.T) {

	iTestRecordSequenceConsistent(t,
		func(sequence *Sequence, records []Record) {
			for i := 0; i < 10; i++ {
				for _, rec := range records {
					sequence.Store(rec)
				}
			}
		})
}

func TestRecord_SequenceConsistentMultiThread(t *testing.T) {

	iTestRecordSequenceConsistent(t,
		func(sequence *Sequence, records []Record) {
			count := 100 //  100 threads!!
			var wg sync.WaitGroup
			wg.Add(count)
			for i := 0; i < count; i++ {
				go func() {
					for _, rec := range records {
						sequence.Store(rec)
					}
					wg.Done()
				}()
			}
			wg.Wait()
		})
}

func iTestRecordSequenceConsistent(t *testing.T, seqProducerFunc func(*Sequence, []Record)) {
	seqLen := 1000
	seq := generateSequence(cid.Undef, seqLen, false)

	// emulate random order with 2x redundancy
	seqCpy := make([]Record, 2*len(seq))
	copy(seqCpy[:len(seq)], seq)
	copy(seqCpy[len(seq):], seq)
	rand.Shuffle(len(seqCpy), func(i, j int) { seqCpy[i], seqCpy[j] = seqCpy[j], seqCpy[i] })

	recs := NewRecordSequence(
		func(r *Record) {
			//fmt.Printf("added: %v\n", r)
		},
	)

	seqProducerFunc(recs, seq)

	collected, ok := recs.List()
	if !ok {
		t.Error("cannot reconstruct consistent record sequence")
	}

	if !equalSequences(collected, seq) {
		t.Errorf("reconstructed sequence doesn't match original one\noriginal: %s\nrestored: %s",
			formatSequence(seq), formatSequence(collected))
	}

	t.Log("passed")
}

func TestRecord_SequenceGaps(t *testing.T) {

	seqLen := 100
	seq := generateSequence(cid.Undef, seqLen, true)
	recs := NewRecordSequence(func(r *Record) {
		//fmt.Printf("added: %v\n", r)
	})

	for _, rec := range seq {
		recs.Store(rec)
	}

	_, ok := recs.List()
	if ok {
		t.Error("expected: List() should have failed, observed: List() succeeded")
	}
	t.Log("passed")
}

func generateSequence(from cid.Cid, size int, forceInvalid bool) []Record {
	var (
		prev = from
		seq  = make([]Record, size)
	)

	for i := 0; i < size; i++ {
		rec := generateRecord([]byte(fmt.Sprintf("record:%d", i)), prev)
		seq[i] = rec
		if !forceInvalid {
			prev = rec.Cid()
		}
	}

	return seq
}

func generateRecord(data []byte, prev cid.Cid) Record {
	mh := util.Hash(data)
	id := cid.NewCidV0(mh)
	return mockRecord{id: id, prevID: prev, data: data}
}

func equalSequences(s1, s2 []Record) bool {
	if len(s1) != len(s2) {
		return false
	}

	for i := 0; i < len(s1); i++ {
		if s1[i].Cid() != s2[i].Cid() || s1[i].PrevID() != s2[i].PrevID() {
			return false
		}
	}

	return true
}

func formatSequence(seq []Record) string {
	var (
		recs      = make([]string, len(seq))
		formatCID = func(id cid.Cid) string {
			if id == cid.Undef {
				return "Undef"
			}
			ir := id.String()
			return fmt.Sprintf("...%s", ir[len(ir)-6:])
		}
	)

	for i, rec := range seq {
		recs[i] = fmt.Sprintf("(Prev: %s, ID: %s)", formatCID(rec.PrevID()), formatCID(rec.Cid()))
	}

	return strings.Join(recs, " -> ")
}
