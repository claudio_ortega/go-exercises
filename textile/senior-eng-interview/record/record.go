package record

import (
	"github.com/ipfs/go-cid"
	"sync"
)

// Record represents an item in a sequence.
type Record interface {
	Cid() cid.Cid
	PrevID() cid.Cid
}

// Sequence holds fragments of a record sequence.
type Sequence struct {
	fragments [][]Record
	set       map[cid.Cid]struct{}
	mutex     *sync.Mutex
	listener  func(*Record)
}

// NewRecordSequence returns a new Sequence.
func NewRecordSequence(listener func(*Record)) *Sequence {
	return &Sequence{
		fragments: nil,
		set:       make(map[cid.Cid]struct{}),
		mutex:     &sync.Mutex{},
		listener:  listener,
	}
}

// Store adds a record to the sequence.
func (s *Sequence) Store(rec Record) {
	s.mutex.Lock()
	s.storeUsafe(rec)
	s.mutex.Unlock()
}

func (s *Sequence) storeUsafe(rec Record) {
	// Verify if record is already contained in some fragment
	if _, found := s.set[rec.Cid()]; found {
		return
	}
	s.set[rec.Cid()] = struct{}{}

	// Now try to find a sequence to be attached to
	for i, fragment := range s.fragments {
		if fragment[len(fragment)-1].Cid() == rec.PrevID() {
			s.fragments[i] = append(fragment, rec)
			s.listener(&rec)
			return
		} else if fragment[0].PrevID() == rec.Cid() {
			s.fragments[i] = append([]Record{rec}, fragment...)
			s.listener(&rec)
			return
		}
	}

	// Start a new fragment
	s.fragments = append(s.fragments, []Record{rec})
	s.listener(&rec)
}

// List returns reconstructed sequence and success flag.
func (s *Sequence) List() ([]Record, bool) {
LOOP:
	// Avoid recursion as sequences could be pretty large
	for {
		if len(s.fragments) == 1 {
			return s.fragments[0], true
		}

		// Take a fragment ...
		fragment := s.fragments[0]
		fHead, fTail := fragment[len(fragment)-1], fragment[0]

		// ...and try to compose it with another one
		for i, candidate := range s.fragments[1:] {
			cHead, cTail := candidate[len(candidate)-1], candidate[0]
			// Index shifted by slicing
			i += 1

			if fHead.Cid() == cTail.PrevID() {
				// Composition: (tail) <- fragment <- candidate <- (head)
				s.fragments[0] = append(fragment, candidate...)
				s.fragments = append(s.fragments[:i], s.fragments[i+1:]...)
				continue LOOP

			} else if fTail.PrevID() == cHead.Cid() {
				// Composition: (tail) <- candidate <- fragment <- (head)
				s.fragments[i] = append(candidate, fragment...)
				s.fragments = s.fragments[1:]
				continue LOOP
			}
		}

		// No composition found, hence there are at least two disjoint fragments
		return nil, false
	}
}
