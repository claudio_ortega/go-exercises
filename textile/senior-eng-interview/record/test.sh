set -ex

go clean -testcache
go build .
go test -v .
