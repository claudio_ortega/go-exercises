package main

import (
	"encoding/json"
	"fmt"
	flag "github.com/spf13/pflag"
	"log"
	"os"
	"path/filepath"
	"regexp"
)

func main() {
	exitCode := 0
	if err := mainAlt(os.Args); err != nil {
		log.Println(err.Error())
		exitCode = 1
	}
	os.Exit(exitCode)
}

func mainAlt(args []string) error {
	flags := flag.NewFlagSet(args[0], flag.ExitOnError)
	showHelpAndQuit := flags.Bool("help", false, "print out this help and quit")
	dryRun := flags.Bool("dry-run", true, "show what would be deleted, but do not delete anything")
	targetDir := flags.String("root-dir", "/Volumes/My Passport/google-drive-local-mirror/folder1", "target directory")
	if err := flags.Parse(os.Args[1:]); err != nil {
		return err
	}
	if *showHelpAndQuit {
		flags.Usage()
		return nil
	}
	return clean(*targetDir, *dryRun)
}

type filterFunction func(path string) bool

type visitorFunction func(path string, info os.FileInfo, filterF filterFunction, st *stats) error

type stats struct {
	Dirs        int
	File        int
	MatchedDir  int
	MatchedFile int
}

func clean(targetDir string, dryRun bool) error {
	fmt.Printf("targetDir:[%s]\n", targetDir)
	fmt.Printf("dryRun:[%t]\n", dryRun)
	rxp := regexp.MustCompile(` \([0-9]+\)`)
	filterFn := func(path string) bool {
		return rxp.MatchString(filepath.Base(path))
	}
	visitorFn := func(path string, info os.FileInfo, filterF filterFunction, st *stats) error {
		if info.IsDir() {
			st.Dirs++
		} else {
			st.File++
		}
		if !filterF(path) {
			return nil
		}
		if dryRun {
			if info.IsDir() {
				st.MatchedDir++
				fmt.Printf("dryrun mode, skipping removal of dir: [%s]\n", path)
			} else {
				st.MatchedFile++
				fmt.Printf("dryrun mode, skipping removal of file: [%s]\n", path)
			}
			return nil
		} else {
			if info.IsDir() {
				st.MatchedDir++
				names, errRead := listFilesUnderDir(path)
				if errRead != nil {
					return errRead
				}
				if len(names) > 0 {
					fmt.Printf("skipping non empty directory: [%s]\n", path)
					return nil
				}
				if err := os.Remove(path); err != nil {
					return err
				}
				fmt.Printf("removed directory: [%s]\n", path)
			} else {
				st.MatchedFile++
				if err := os.Remove(path); err != nil {
					return err
				}
				fmt.Printf("removed file: [%s]\n", path)
			}
		}
		return nil
	}
	st := &stats{}
	if err := walk(targetDir, visitorFn, filterFn, st); err != nil {
		return err
	}
	b, _ := json.MarshalIndent(st, "", "    ")
	fmt.Printf("stats: %s\n", string(b))
	return nil
}

func walk(path string, visitFn visitorFunction, filterFn filterFunction, st *stats) error {
	info, err := os.Lstat(path)
	if err != nil {
		return err
	}
	// self plain file
	if !info.IsDir() {
		return visitFn(path, info, filterFn, st)
	}
	// directory
	names, errRead := listFilesUnderDir(path)
	if errRead != nil {
		return errRead
	}
	// go deep first
	for _, name := range names {
		errWalk := walk(filepath.Join(path, name), visitFn, filterFn, st)
		if errWalk != nil {
			return errWalk
		}
	}
	// self directory at last
	return visitFn(path, info, filterFn, st)
}

func listFilesUnderDir(dirname string) ([]string, error) {
	f, err := os.Open(dirname)
	if err != nil {
		return nil, err
	}
	defer func() {
		errd := f.Close()
		if errd != nil {
			fmt.Printf("error closing directory %s\n", dirname)
		}
	}()
	names, err := f.Readdirnames(-1)
	if err != nil {
		return nil, err
	}
	return names, nil
}
